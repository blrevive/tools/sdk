C++ SDKs for Blacklight: Retribution. Created by using a custom version of TheFeckless' [UnrealSDKGenerator]().



The actual source code only exists in other branches than `master`. 

## Branches

- [3.02](https://gitlab.com/blrevive/tools/sdk/-/tree/3.02): This branch contains a SDK for the latest BL:R client.
